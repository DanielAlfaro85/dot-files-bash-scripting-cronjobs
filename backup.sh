#!/bin/bash
echo "Iniciando resplado.."

site="lfts.isw811.xyz"
directory="/home/vagrant/backups/${site}/"
datetime=$(date +"%Y%m%d_%H%M%S")
database="blogs"
username="laravels"
password="secret"

if [ ! -z "$1" ]; then
    filename="${site}_${1}.sql"
else
    filename="${site}_${datetime}.sql"
fi

mkdir -p $directory
 mysqldump $database > "${directory}/${filename}" -u $username --password=$password

cd $directory
tar cvfz "${filename}.tar.gz" "${filename}"

echo "Eliminando archivo temporal"
rm $filename 


echo "Registrando la ejecución"
echo $filename $(date +"%d/%m/%Y  %H:%M:%S") >> backup_$site.log


echo "Listo"
