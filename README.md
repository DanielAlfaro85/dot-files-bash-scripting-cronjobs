# Dot files Bash scripting Cronjobs 


## Dot files
Los dot files son archivos que se encuentran en el sistema para la ejecución de programas 
en debian algunos de los que encontramos son _.bashrc_ y _.profile_ 

## Bash scripting
Creando el archivo _sayhello_. Basicamente aqui creamos un archivo que a la hora de ejcutarlo nos pide
el nombre y la fecha de nacimiento, y si no se ingresa un número vuelve a pedir la fecha de nacimiento.
```sh
#!/bin/bash

if [ -z "$1" ]; then 
    echo "Hola amigo!"
else
    echo "Hola $1"
fi

while [ -z `echo $birthday | grep "^[0-9]*$"` ]; do
    printf "¿En cuál año nacio usted? "
    read birthday

    if [ -z `echo $birthday | grep "^[0-9]*$"` ]; then
        echo "Debe ingresar un número"
    fi
done

```
Creacion de archivo _backup_. Aqui crearemos un archivo para que a la hora de ejecutarlo realice un backup de lfts.isw811.xyz

```sh
#!/bin/bash
echo "Iniciando resplado.."

site="lfts.isw811.xyz"
directory="/home/vagrant/backups/${site}/"
datetime=$(date +"%Y%m%d_%H%M%S")
database="blogs"
username="laravels"
password="secret"

if [ ! -z "$1" ]; then
    filename="${site}_${1}.sql"
else
    filename="${site}_${datetime}.sql"
fi

mkdir -p $directory
 mysqldump $database > "${directory}/${filename}" -u $username --password=$password

cd $directory
tar cvfz "${filename}.tar.gz" "${filename}"

echo "Eliminando archivo temporal"
rm $filename 


echo "Registrando la ejecución"
echo $filename $(date +"%d/%m/%Y  %H:%M:%S") >> backup_$site.log


echo "Listo"

```


### Cronjob
Los cronjobs son tareas que se ejecutan como los programamos. Los minutos, hora, dia del mes, mes, dia de la semana se representan con los `*` 
En esta parte agarraremos el ejemplo del backup y lo programaremos para que se ejecute automaticamente

* * * * * /home/vagrant/backups/lfts.isw811.xyz/backup.sh

